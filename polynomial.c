#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#include "polynomial.h"

int
main(void)
{
    polynomial *poly1 = term_create(1, 2);

    poly1->next = term_create(1, 2);
    poly1->next->next = term_create(1, 1);
    poly1->next->next->next = term_create(1, 1);

    poly_print(poly1);
    printf("\n");

    printf("Evaluate x = 2:%lf\n", poly_eval(poly1, 2));
    polynomial *poly2 = term_create(1, 4);

    poly2->next = term_create(1, 2);
    poly2->next->next = term_create(1, 1);
    poly2->next->next->next = term_create(1, 0);
    poly_print(poly2);
    printf("\n");

    printf("List First and List Second Are equal: %s\n",
           poly_equal(poly1, poly2) ? "True" : "False");

    char *string = poly_to_string(poly1);

    printf("String:\n");
    printf("%s\n", string);

    /*polynomial *poly3 = poly_sub(poly1,poly2);
    poly_print(poly3);
    printf("\n");
    poly_destroy(&poly3)*/

    polynomial *poly4 = poly_mult(poly1,poly2);
    poly_print(poly4);
    printf("\n");

    // Death to the mallocs
    poly_destroy(&poly4);
    poly_destroy(&poly1);
    poly_destroy(&poly2);
    free(string);
}



polynomial * term_create(int coeff, unsigned int exp)
{
    // used to create a polynomial term. code was given to us
    polynomial *node = malloc(sizeof(*node));

    if (node)
    {
        node->coeff = coeff;
        node->exp = exp;
        node->next = NULL;
    }
    return node;
}

void
insert_term(polynomial ** front, int newCoeff, int newExp)
{
    // create new node
    polynomial *insert = (polynomial *) malloc(sizeof(polynomial));

    if (!insert)
    {
        printf("Memory Error\n");
        return;
    }
    // insert data into new node
    insert->coeff = newCoeff;
    insert->exp = newExp;
    insert->next = NULL;

    // connect to list
    insert->next = *front;
    *front = insert;
}


void
poly_simplify(polynomial ** a)
{
    // Combines consecutive occurances of polynomial
    polynomial *prev = *a;
    polynomial *cursor = (*a)->next;

    while (cursor)
    {
        // Combinig the consecutive occurances
        if (prev->exp == cursor->exp)
        {
            prev->coeff += cursor->coeff;
            prev->next = cursor->next;
            cursor->next = NULL;
            free(cursor);
            cursor = prev->next;

            if (!prev->coeff)
            {
                *a = cursor;
                cursor = cursor->next;
                prev->next = NULL;
                free(prev);

                prev = *a;
            }
        }
        // Moving on didn't find anything
        else
        {
            prev = cursor;
            cursor = cursor->next;
        }
    }
}

int
poly_length(polynomial * a)
{
    // code pulled from excersises given in class
    polynomial *cursor = a;
    int length = 0;

    while (cursor != NULL)
    {
        ++length;
        cursor = cursor->next;
    }
    return length;
}

void
poly_destroy(polynomial ** eqn)
{
    // code pulled from excersises given in class
    polynomial *cursor = *eqn;

    while (cursor)
    {
        *eqn = cursor->next;
        cursor->next = NULL;
        free(cursor);
        cursor = *eqn;
    }
}

void
poly_print(polynomial * eqn)
{
    // code was given to us
    // Always be checking!?
    if (!eqn)
    {
        return;
    }

    // Simplify before acting on the data
    poly_simplify(&eqn);
    static bool firstTerm = true;

    if (eqn->coeff)
    {
        // Ignore + in first check to print a +, else print it
        if (firstTerm)
        {
            printf("%d", eqn->coeff);
            firstTerm = false;
        }
        else
        {
            printf("%c%d", eqn->coeff > 0 ? '+' : '\0', eqn->coeff);

        }

        if (eqn->exp > 1)
        {
            printf("x^%d", eqn->exp);
        }
        else if (eqn->exp == 1)
        {
            printf("x");
        }
        // Space out each term for increased readability
        printf(" ");
    }
    // Recusion was given to us Liam, even if you don't like it.
    poly_print(eqn->next);

    // reset firstTerm to true for other calls to poly_print
    firstTerm = true;
}

char *
poly_to_string(polynomial * p)
{
    if (!p)
    {
        return 0;
    }

    // Simplify the polynomial before acting on it
    poly_simplify(&p);

    // Going to need to intialize some variables
    char *polyString = calloc(6 * poly_length(p) + 1, sizeof(char));;
    char buff[4];
    polynomial *cursor = p;
    bool firstTerm = true;

    //Using poly_print logic to create string
    while (cursor != NULL)
    {
        if (cursor->coeff)
        {
            if (firstTerm)
            {
                firstTerm = false;
            }
            else
            {
                // Writing sign and coefficent to the string
                snprintf(buff, 4, "%c", (cursor->coeff > 0 ? '+' : '\0'));
                strncat(polyString, buff, 1);
            }

            snprintf(buff, 4, "%d", cursor->coeff);
            strncat(polyString, buff, 4);

            // Writing X^, X, or nothing (X^0) to string
            if (cursor->exp > 1)
            {
                strncat(polyString, "x^", 2);
                snprintf(buff, 4, "%d", cursor->exp);
                strncat(polyString, buff, 4);
            }
            else if (cursor->exp == 1)
            {
                strncat(polyString, "x", 2);
            }
            // Space out each term for increased readability
            strncat(polyString, " ", 2);
        }
        cursor = cursor->next;
    }
    return polyString;
}

polynomial * poly_add(polynomial * a, polynomial * b)
{
    // Lets simplify before acting on the data given
    poly_simplify(&a);
    poly_simplify(&b);

    // Code for poly add based on code created by S. Cuillo
    polynomial *head = term_create(0, 0);
    polynomial *result = head;
    polynomial *prev = head, *prev2 = head;

    head->next = NULL;
    polynomial *temp1 = a, *temp2 = b;

    while (temp1 != NULL && temp2 != NULL)
    {
        // temp1 greater than temp2, store temp1 in result list and continue
        if (temp1->exp > temp2->exp)
        {
            head->next = temp1;
            temp1 = temp1->next;
        }
        // if they are equal add together and free any necessary mallocs
        else if (temp1->exp == temp2->exp)
        {
            head->next =
                term_create((temp1->coeff + temp2->coeff), temp1->exp);
            prev = temp1;
            temp1 = temp1->next;
            free(prev);
            prev2 = temp2;
            temp2 = temp2->next;
            free(prev2);

        }
        // getting here means temp2 greater than temp1 so store temp1
        else
        {
            head->next = temp2;
            temp2 = temp2->next;
        }
        head = head->next;
    }

    // This if/ else if handles if one list runs out before the other
    if (temp1 != NULL)
    {
        // Adding if temp1 is the longest list
        while (temp1 != NULL)
        {
            head->next = temp1;
            temp1 = temp1->next;
            head = head->next;
        }
    }
    else if (temp2 != NULL)
    {
        // Adding if temp2 is the longest list
        while (temp2 != NULL)
        {
            head->next = temp2;
            temp2 = temp2->next;
            head = head->next;
        }
    }

    // Clean up. Must. Free. Mallocs
    head->next = NULL;
    poly_destroy(&temp1);
    poly_destroy(&temp2);
    return result;

}

polynomial * poly_sub(polynomial * a, polynomial * b)
{
    // Subtraction will be the same as add except for subrating coefficents
    // versus adding coefficents.
    //poly_simplify(&a);
    //poly_simplify(&b);

    polynomial *head = term_create(0, 0);
    polynomial *result = head;
    polynomial *prev = head, *prev2 = head;

    head->next = NULL;
    polynomial *temp1 = a, *temp2 = b;

    while (temp1 != NULL && temp2 != NULL)
    {
        // temp1 greater than temp2, store temp1 in result list and continue
        if (temp1->exp > temp2->exp)
        {
            head->next = temp1;
            temp1 = temp1->next;
        }
        // if exp are equal subtract(really adding negative of temp2 to get
        // around sign issues with first term in polynomial)
        else if (temp1->exp == temp2->exp)
        {
            head->next =
                term_create((temp1->coeff + (-1 * temp2->coeff)), temp1->exp);
            prev = temp1;
            temp1 = temp1->next;
            free(prev);
            prev2 = temp2;
            temp2 = temp2->next;
            free(prev2);

        }
        // getting here means temp2 greater than temp1 so store temp1
        else
        {
            //need to swap sign if temp2 is just stored in list
            temp2->coeff *= -1;
            head->next = temp2;
            temp2 = temp2->next;
        }
        head = head->next;
    }
    if (temp1 != NULL)
    {
        while (temp1 != NULL)
        {
            head->next = temp1;
            temp1 = temp1->next;
            head = head->next;
        }
    }
    else if (temp2 != NULL)
    {
        while (temp2 != NULL)
        {
            //need to swap sign if temp2 is just stored in list
            temp2->coeff *= -1;
            head->next = temp2;
            temp2 = temp2->next;
            head = head->next;
        }
    }
    // Clean up. Must. Free. Mallocs
    head->next = NULL;
    poly_destroy(&temp1);
    poly_destroy(&temp2);
    return result;

}

double
poly_eval(polynomial * a, double elavuate)
{
    // Evaluate is just substituting the double in for X everywhere
    // in the polynomial
    poly_simplify(&a);
    polynomial *cursor = a;
    double result = 0;

    while (cursor != NULL)
    {
        // Continuous sum of the evaluation 
        // constant is still evaluated since it is stored with an exp of 0 
        result += cursor->coeff * pow(elavuate, cursor->exp);
        cursor = cursor->next;
    }
    free(cursor);
    return result;
}

bool
poly_equal(polynomial * a, polynomial * b)
{
    // Testing if polynomials are equal
    // Simplifying polynomials before checking for equality
    poly_simplify(&a);
    poly_simplify(&b);

    polynomial *cursorA = a;
    polynomial *cursorB = b;

    // They can't be equal if they are differnt lengths
    if (poly_length(cursorA) != poly_length(cursorB))
    {
        return false;
    }

    while (cursorA != NULL)
    {
        if (cursorA->exp == cursorB->exp)
        {
            if (cursorA->coeff == cursorB->coeff)
            {
                // Increment and continue if exp and coeff are equal
                cursorA = cursorA->next;
                cursorB = cursorB->next;
                // Only continue if both are still equal
                continue;
            }
        }
        // Coeff and exp are both not equal
        // pull the chute and bail. Polynomials are not equal
        return false;
    }
    // Made it this far they are equal.
    return true;
}

void
poly_iterate(polynomial * p, void (*transform) (struct term *))
{
    poly_simplify(&p);
    polynomial *term = p;

    // Push the term to the iterate function the user wants
    while (term != NULL)
    {
        transform(term);
        term = term->next;
    }
}

void
ll_reverse(polynomial **a)
{
    // Reverse Reverse. Everybody clap your hands.
    // Sometimes you just need to reverse the order of your list.
    // Taken from code I wrote during class, which is why it is named
    // ll_reverse
    if (poly_length(*a)<= 1)
    {
        // If length 1, the reverse is itself
        return;
    }
    else if (poly_length(*a)== 2)
    {
        // length of two does not need a loop to reverse.
        polynomial *cursor = *a;
        polynomial *forward=cursor->next;
        cursor->next = NULL;
        forward->next = cursor;
        *a = forward;        
    }
    // Only need a generic reverse algarithom when length > 2
    else
    {
        // Must. Initilize. Pointers...
        polynomial *previous = *a;
        polynomial *cursor= previous->next;
        polynomial *forward = cursor->next;
        previous->next = NULL;
        while (true)
        {
            // Pointing backwards while still mainting a handle on the 
            // next object I just removed a link from
            cursor->next = previous;
            previous = cursor;
            cursor = forward;
            forward = forward->next;
            if (forward->next == NULL)
            {
                // next == NULL, then we are done
                cursor->next = previous;
                forward->next = cursor;
                *a = forward;
                break;
            }
        }
    }
}

polynomial
*poly_mult(polynomial *a, polynomial *b)
{
    // Thank you to A Dow for helping to find the two memory leaks in
    // this function, that where aluding me for hours.
    poly_simplify(&a);
    poly_simplify(&b);

    polynomial *cursorA = a;
    polynomial *cursorB = b;
    polynomial *tempCursorB = b;
    polynomial *product = term_create(0, 0);
    polynomial *temp;

    int coeff = 0;
    unsigned int exp = 0;
    bool first = true;

    while (cursorA != NULL)
    {
        while (cursorB != NULL)
        {
            // Calulate Coefficent and exp for each new term 
            // Mulitplying polynomials will create n*m number of new terms
            coeff = cursorA->coeff * cursorB->coeff;
            exp = cursorA->exp + cursorB->exp;

            // Create the new term

            if (first == true)
            {
                // If it is the first term, justmake that the new polynomial
                temp = term_create(coeff, exp);
                polynomial *blank = product;
                product = temp;
                first = false;
                product->next = NULL;
                free(blank);
                
            }
            else
            {
                // But if there already exits a product polynomial
                // just add the new term to the polynomial
                // poly_add will place it in the correct space and add terms
                // as necessary.
                temp = term_create(coeff, exp);
                polynomial *productReturn = poly_add(product, temp);
                free(productReturn);
            }
            cursorB = cursorB->next;
        }
        cursorB = tempCursorB;
        cursorA = cursorA->next;
    }    
    return product;
}
