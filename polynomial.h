typedef struct term
{
    int coeff;
    unsigned int exp;
    struct term *next;
}polynomial;

polynomial
*term_create(int coeff,unsigned int exp);

void
poly_destroy(polynomial **eqn);

void
poly_print(polynomial *eqn);

void
insert_term(polynomial **front,int newCoeff, int newExp);

int
poly_length(polynomial *a);

void
poly_simplify(polynomial **a);

char
*poly_to_string(polynomial *p);

polynomial
*poly_add(polynomial *a, polynomial *b);

polynomial
*poly_sub(polynomial *a, polynomial *b);

double
poly_eval(polynomial *a, double elavuate);

bool
poly_equal(polynomial *a, polynomial *b);

void
poly_iterate(polynomial *p, void(*transform)(struct term *));

void
ll_reverse(polynomial **a);

polynomial
*poly_mult(polynomial *a, polynomial *b);
